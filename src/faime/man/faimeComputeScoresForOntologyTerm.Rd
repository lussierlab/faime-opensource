\name{faimeComputeScoresForOntologyTerm}
\alias{faimeComputeScoresForOntologyTerm}
\title{Computes FAIME score for parameter ontology term at each sample}
\usage{
  faimeComputeScoresForOntologyTerm(ontologyId,
    ontologyParams)
}
\arguments{
  \item{ontologyId}{Identifier key of ontology term to
  compute FAIME scores for}

  \item{ontologyParams}{A list of read-only data including:
  \itemize{ \item{\code{ontologyParams$ontology} - A
  one-to-one mapping matrix with two columns, the 1st
  column is an ontology id (i.e. GO/KEGG term), and the 2nd
  is a gene id associated with that term}
  \item{\code{ontologyParams$geneIdsCapturedByProbes} - The
  set of gene IDs that are captured by this experiment}
  \item{\code{ontologyParams$probeIds} - The identifiers
  for all probes used in this experiment}
  \item{\code{ontologyParams$transforedExpData} -
  Experiment data that has had transform applied to it}
  \item{\code{ontologyParams$reduce} - A function used for
  scoring an ontology term for each sample such as
  \code{\link{faimeReduce.CentroidDistance}}} }}
}
\value{
  FAIME scores for each sample for this parameter ontology
}
\description{
  Computes FAIME score for parameter ontology term at each
  sample
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

