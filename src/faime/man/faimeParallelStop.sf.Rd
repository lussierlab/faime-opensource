\name{faimeParallelStop.sf}
\alias{faimeParallelStop.sf}
\title{Stops parallelization library}
\usage{
  faimeParallelStop.sf(handle, ...)
}
\arguments{
  \item{handle}{A list containing the parallel policy
  interface created via
  \code{\link{faimeParallelCreateHandle.sf}}}

  \item{\dots}{Additonal arguments for stopping the
  parallelization library}
}
\value{
  Result of stopping parallelization library
}
\description{
  Stops parallelization library
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

