\name{faimeTransform.Ranked}
\alias{faimeTransform.Ranked}
\title{Ranks expression data}
\usage{
  faimeTransform.Ranked(expData, hpc, na.last = TRUE)
}
\arguments{
  \item{expData}{An expression matrix (or data.frame), row
  for a gene and column for a sample}

  \item{hpc}{A handle to a parallel policy interface}

  \item{na.last}{See \code{\link{rank}} for controlling the
  treatment of NAs}
}
\value{
  Ranked gene expression data
}
\description{
  Ranks expression data
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

