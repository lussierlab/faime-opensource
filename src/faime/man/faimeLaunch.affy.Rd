\name{faimeLaunch.affy}
\alias{faimeLaunch.affy}
\title{Launches FAIME for Affymetrix using data files specified by parameter paths}
\usage{
  faimeLaunch.affy(pathToExpressionData = stop("'pathToExpressionData' must be specified"),
    pathToOntology = stop("'pathToOntology' must be specified"),
    pathToOutputData = NULL,
    transform = faimeTransform.RankedNegExp,
    reduce = faimeReduce.CentroidDistance, hpc = NULL,
    logCheck = FALSE, na.last = TRUE)
}
\arguments{
  \item{pathToExpressionData}{Path to expression data file
  (currently only .RData format accepted)}

  \item{pathToOntology}{Path to ontology data file
  (currently only text format supported)}

  \item{pathToOutputData}{Optional path to write FAIME
  result to}

  \item{transform}{A function to transform expression data
  for each sample such as
  \code{\link{faimeTransform.RankedNegExp}},
  \code{\link{faimeTransform.RankedPosExp}}, or
  \code{\link{faimeTransform.Ranked}}}

  \item{reduce}{A function used for scoring an ontology
  term for each sample such as
  \code{\link{faimeReduce.CentroidDistance}}}

  \item{hpc}{Optional handle to high performance computing
  interface}

  \item{logCheck}{If TRUE, will log2 transform expression
  values if they exceed a threshold}

  \item{na.last}{See \code{\link{rank}} for controlling the
  treatment of NAs}
}
\value{
  A matrix with FAIME scores for each ontology keyword
  (rows) and sample (columns)
}
\description{
  Launches FAIME for Affymetrix using data files specified
  by parameter paths
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

