################################################################################################################################################
# Collection of tests to verify Affymetric FAIME transforms
################################################################################################################################################

#################################
# Context
#################################

context("Affymetrix")

#################################
# Libraries
#################################

# To install, type 'install.packages("testthat")' from within R prompt
library("testthat")

# To install Bioconductor three packages, include the following in your R code.
# source("http://bioconductor.org/biocLite.R")
# biocLite("hgu95av2.db")
library("hgu95av2.db")

#################################
# Sources
#################################

# Load test configuration
source("config.R")

# Load test template utilites
source("template.R")

#################################
# Affy Utilities
#################################

#' Utility appends Affymetrix specific test parameters:
#' - probeIdToGeneIdMap
#' - dataSetType
#' @param testParams Parameters that will be modified
#' @return \code{testParams} with added attributes
appendTestParams.affy <- function(testParams) {
  # Get the probe identifiers that are mapped to a gene symbol
  mappedProbeIds <- mappedkeys(hgu95av2SYMBOL) 
  # Create a list mapping from probe identifier to gene identifier
  testParams$probeIdToGeneIdMap <- as.list(hgu95av2SYMBOL[mappedProbeIds])
  testParams$dataSetType <- "affy"
  return(testParams)
}

#' Utility appends GSE6631 MAS5 Cropped specific test parameters:
#' - dataSetName
#' - dataSetMods
#' @param testParams Parameters that will be modified
#' @return \code{testParams} with added attributes
appendTestParams.GSE6631.Mas5.Cropped <- function(testParams) {
  testParams$dataSetName <- "GSE6631"
  testParams$dataSetMods <- "-mas5-cropped"
  return(testParams)
}

#' Utility to generate canonical Affymetrix data to use in validation
#' @param testParams List containing the following members:
#'   \itemize{
#'     \item{\code{testParams$dataSetType} type of data set (e.g. - "affy", "rnaSeq")}
#'     \item{\code{testParams$dataSetName} - name of data set (e.g. - "GSE6631")}
#'     \item{\code{testParams$dataSetMods} - modifiers to the base data set (e.g. - "-mas5-cropped")}
#'     \item{\code{testParams$ontologyPath} - path to ontology gene mapping}
#'     \item{\code{testParams$transform} - the type of weight transform to use}
#'     \item{\code{testParams$transformName} - the name of the weight transform to use}
#'     \item{\code{testParams$probeIdToGeneIdMap} - maps row names of data set to a gene ID}
#'   }
#' @return TRUE
generateCanonicalData.affy <- function(testParams) {
  generateCanonicalData(appendTestParams.affy(testParams))
}

#' Utility to generate canonical Affymetrix data to use in validation
#' - for GSE6631 MAS 5 Cropped with a negative exponent transform 
#'
#' @param testParams List containing the following members:
#'   \itemize{\item{\code{testParams$ontologyPath} - path to ontology gene mapping}}
#' @return TRUE
generateCanonicalData.affy.GSE6631.Mas5.Cropped.TrNegExp <- function(testParams) {
  generateCanonicalData.affy(appendTestParams.GSE6631.Mas5.Cropped(appendTestParams.TrNegExp(testParams)))
}

#' Utility to generate canonical Affymetrix data to use in validation
#' - for GSE6631 MAS 5 Cropped with a positive exponent transform 
#'
#' @param testParams List containing the following members:
#'   \itemize{\item{\code{testParams$ontologyPath} - path to ontology gene mapping}}
#' @return TRUE
generateCanonicalData.affy.GSE6631.Mas5.Cropped.TrPosExp <- function(testParams) {
  generateCanonicalData.affy(appendTestParams.GSE6631.Mas5.Cropped(appendTestParams.TrPosExp(testParams)))
}

#' Utility to run a test on Affymetrix data
#' @param testParams List containing the following members:
#'   \itemize{
#'     \item{\code{testParams$dataSetType} type of data set (e.g. - "affy", "rnaSeq")}
#'     \item{\code{testParams$dataSetName} - name of data set (e.g. - "GSE6631")}
#'     \item{\code{testParams$dataSetMods} - modifiers to the base data set (e.g. - "-mas5-cropped")}
#'     \item{\code{testParams$ontologyPath} - path to ontology gene mapping}
#'     \item{\code{testParams$transform} - the type of weight transform to use}
#'     \item{\code{testParams$transformName} - the name of the weight transform to use}
#'     \item{\code{testParams$probeIdToGeneIdMap} - maps row names of data set to a gene ID}
#'   }
#' @return TRUE if generated FAIME result matches canonical set
runTest.affy <- function(testParams) {
  runTest(appendTestParams.affy(testParams))
}

#' Utility to run a test on GSE6631 MAS5 Cropped Affymetrix data
#' - with a negative exponent transform 
#' @param testParams List containing the following members:
#'   \itemize{\item{\code{testParams$ontologyPath} - path to ontology gene mapping}}
#' @return TRUE if generated FAIME result matches canonical set
runTest.affy.GSE6631.Mas5.Cropped.TrNegExp <- function(testParams) {
  runTest.affy(appendTestParams.GSE6631.Mas5.Cropped(appendTestParams.TrNegExp(testParams)))
}

#' Utility to run a test on GSE6631 MAS5 Cropped Affymetrix data
#' - with a positive exponent transform 
#' @param testParams List containing the following members:
#'   \itemize{\item{\code{testParams$ontologyPath} - path to ontology gene mapping}}
#' @return TRUE if generated FAIME result matches canonical set
runTest.affy.GSE6631.Mas5.Cropped.TrPosExp <- function(testParams) {
  runTest.affy(appendTestParams.GSE6631.Mas5.Cropped(appendTestParams.TrPosExp(testParams)))
}

#################################
# Tests
#################################

######################################################################
# Verifies that FAIME results for GSE6631 dataset are consistent 
######################################################################

#################################
# GO Molecular Function
#################################

test_that( "GSE6631.Mas5.Cropped.GOMF.TrNegExp matches canonical", {
  expect_true(runTest.affy.GSE6631.Mas5.Cropped.TrNegExp(
    appendTestParams.GOMF(list())))
})

test_that( "GSE6631.Mas5.Cropped.GOMF.TrPosExp matches canonical", {
  expect_true(runTest.affy.GSE6631.Mas5.Cropped.TrPosExp(
    appendTestParams.GOMF(list())))
})

#################################
# GO Biological Pathways
#################################

test_that( "GSE6631.Mas5.Cropped.GOBP.TrNegExp matches canonical", {
  expect_true(runTest.affy.GSE6631.Mas5.Cropped.TrNegExp(
    appendTestParams.GOBP(list())))
})

test_that( "GSE6631.Mas5.Cropped.GOBP.TrPosExp matches canonical", {
  expect_true(runTest.affy.GSE6631.Mas5.Cropped.TrPosExp(
    appendTestParams.GOBP(list())))
})

#################################
# GO Biological Pathways (500-5)
#################################

test_that( "GSE6631.Mas5.Cropped.GOBP.500.5.TrNegExp matches canonical", {
  expect_true(runTest.affy.GSE6631.Mas5.Cropped.TrNegExp(
    appendTestParams.GOBP.500.5(list())))
})

test_that( "GSE6631.Mas5.Cropped.GOBP.500.5.TrPosExp matches canonical", {
  expect_true(runTest.affy.GSE6631.Mas5.Cropped.TrPosExp(
    appendTestParams.GOBP.500.5(list())))
})

#################################
# KEGG
#################################

test_that( "GSE6631.Mas5.Cropped.KEGG.TrNegExp matches canonical", {
  expect_true(runTest.affy.GSE6631.Mas5.Cropped.TrNegExp(
    appendTestParams.KEGG(list())))
})

test_that( "GSE6631.Mas5.Cropped.KEGG.TrPosExp matches canonical", {
  expect_true(runTest.affy.GSE6631.Mas5.Cropped.TrPosExp(
    appendTestParams.KEGG(list())))
})
