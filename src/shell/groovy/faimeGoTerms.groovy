/**
 * Parses .obo file and creates mapping from GO identifier to meta-information
 */
public class faimeGoTerms extends faimeSerializableKeyStore {

    /**
     * Default constructor
     */
    public faimeGoTerms() {
    }

    /**
     * Constructor which initializes terms from file
     * @param oboFilePath Path to gene ontology .obo file
     */
    public faimeGoTerms( String oboFilePath ) {
        parse( oboFilePath )
    }

    /**
     * Populates ontology terms from .obo file
     * @param oboFilePath Path to gene ontology .obo file
     */
    public void parse( String oboFilePath ) {
        def termInfoRegExp = /(.+?): (.+)/
        boolean isSearchingForTerm = true
        String goId = null
        keyStore = [:]
        // Parse each line of .obo file
        new File(oboFilePath)?.eachLine { line ->
            // Wait until we find '[Term]' token before processing term info
            if ( isSearchingForTerm ) {
                isSearchingForTerm = !line.contains( "[Term]" ) 
            } else {
                def match = ( line =~ termInfoRegExp )
                if ( match ) {
                    // Extract (key, value) pairs from term line
                    String key = match[0][1].trim()
                    String val = match[0][2].trim()
                    // If key is 'id', then we need to create a new term mapping
                    if ( key == 'id' ) {
                        goId = val
                        keyStore[ goId ] = [:]
                    } else {
                        // Else, store (key, value) pair for this GO identifier
                        assert goId
                        keyStore[ goId ][ key ] = val
                    }
                } else {
                    // We didn't match a term line, start searching for next term
                    isSearchingForTerm = true
                    goId = null
                }
            }
        }
    }

}

