/**
 * Parses GAF files and creates mapping from GO identifier to list of gene identifiers
 */
public class faimeGoGenes extends faimeSerializableKeyStore {

    // Enumerations for GO annotation file columns
    // See http://www.geneontology.org/GO.format.gaf-2_0.shtml
    private static enum GafColumn {
        DB(0),
        DB_OBJECT_ID(1),
        DB_OBJECT_SYMBOL(2),
        QUALIFIER(3),
        GO_ID(4)
        private final int index
        GafColumn(int index) { this.index = index }
        public int index() { return index }
    }

    /**
     * Default constructor
     */
    public faimeGoGenes() {
    }

    /**
     * Constructor which initializes gene mapping from file
     * @param gafFilePath Path to gene annotation file
     */
    public faimeGoGenes( String gafFilePath ) {
        parse( gafFilePath )
    }

    /**
     * Populates gene mapping from file
     * @param gafFilePath Path to gene annotation file
     */
    public void parse( String gafFilePath ) {
        keyStore = [:]
        // Parse each line of .obo file
        new File( gafFilePath )?.eachLine { line ->
            line = line.trim()
            // Skip empty lines and comments (begin with !)
            if ( ( line.size() > 0 ) && ( line[0] != '!' ) ) {
                // Tokenize line
                def tokens = line.split( '\t' )*.trim()
                String goId = tokens[ GafColumn.GO_ID.index() ]
                String geneId = tokens[ GafColumn.DB_OBJECT_SYMBOL.index() ].split()[0]
                if ( !keyStore.get( goId ) ) {
                    keyStore[ goId ] = []
                }
                keyStore[ goId ] << geneId
            }
        }
    }
}

