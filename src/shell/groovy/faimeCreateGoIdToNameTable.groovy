/**
 * Creates a table for use in R that maps GO identifiers to GO names
 * - Requires .obo file to create a mapping from ontology IDs to ontology meta info
 */

// Parameters

final String dateOfOntology = "6_14_12"
final String nameOfGoIdToNameTable = "GO_ID_TO_NAME_TABLE_${dateOfOntology}"
final String pathToResourcesDir = "../resources/ontologies"
final String pathToOboFile = "${pathToResourcesDir}/gene_ontology_ext.obo"
final String pathToOutputDir = pathToResourcesDir
final String pathToOutputFile = "${pathToOutputDir}/${nameOfGoIdToNameTable}.txt"

// Parse GO terms
final def GO_TERMS = new faimeGoTerms( pathToOboFile )

// Output ontology formatted for use with FAIME
if ( GO_TERMS ) {
    new File( pathToOutputFile ).withWriter { out ->
        out << "GOID\tName\n"
        GO_TERMS.each { goId, info ->
            out << "${goId}\t${info?.name}\n"
        }
    }
}

