################################################################################################################################################
# Research Pipeline: Generate FET Scores
################################################################################################################################################

# --------------
# Sources

source("./src/shell/R/configPaths.R")
source("./src/shell/R/runFet.R")
source("./src/shell/R/runSam.R")
source("./src/shell/R/utilsAffy.R")
source("./src/shell/R/utilsIlmn.R")
source("./src/shell/R/utilsGo.R")
source("./src/shell/R/utilsKegg.R")

# --------------
# Parameters

# Mask used to filter which data sets to run FAIME
DATA.SET.MASK <- c("GSE2379"=TRUE,
                   "GSE6631"=TRUE,
                   "E-MEXP-44"=TRUE,
                   "GSE36968"=TRUE,
                   "GSE13861"=TRUE)

# The names of the datasets
DATA.SET.NAMES <- names(DATA.SET.MASK[DATA.SET.MASK == TRUE])

# Modifiers for each dataset
DATA.SET.MODS <- c("GSE2379"="-filtered",
                   "GSE6631"="-filtered",
                   "E-MEXP-44"="-filtered",
                   "GSE36968"="-rpkm",
                   "GSE13861"="-norm")

# The probe maps used by each data set
DATA.SET.PROBE.MAP.KEYS <- c("GSE2379"="affy",
                             "GSE6631"="affy",
                             "E-MEXP-44"="affy",
                             "GSE36968"="none",
                             "GSE13861"="ilmn")

# The actual probe maps resident in memory
PROBE.MAPS <- list(affy = getProbeIdToGeneIdMap.affy(),
                   ilmn = getProbeIdToGeneIdMap.ilmn(),
                   none = NULL)

# Mask used to filter out which ontologies to use
ONTOLOGY.MASK <- c("KEGG"=TRUE,
                   "GO_molecular_function_6_14_12"=TRUE,
                   "GO_biological_process_6_14_12"=TRUE,
                   "GOMF-gene"=TRUE,
                   "GOBP-gene"=TRUE,
                   "GOBP-gene-500-5"=TRUE)

# The names of the ontologies to use
ONTOLOGY.NAMES <- names(ONTOLOGY.MASK[ONTOLOGY.MASK == TRUE])

# Keys specifying which ontology path id to path name mapping to use
ONTOLOGY.PATHID2PATHNAME.FUNC.KEYS <- c("KEGG"="kegg",
                                        "GO_molecular_function_6_14_12"="go",
                                        "GO_biological_process_6_14_12"="go",
                                        "GOMF-gene"="go",
                                        "GOBP-gene"="go",
                                        "GOBP-gene-500-5"="go")

# Functions for mapping path identifiers to path names
PATHID2PATHNAME.FUNCS <- list(go=goId2Name,
                              kegg=keggId2Name)

# The various false discovery rate thresholds for SAM
SAM.FDRS.MAX <- c(0.01,
                  0.05)

# The test statistics to use 
SAM.TEST.STATISTICS <- c("standard",
                         "wilcoxon")

# The number of permutations to use for FDR estimation
SAM.NPERMS <- c(1000)

# The various false discovery rate thresholds for FET
FET.FDRS.MAX <- c(0.45)

# The various p-value adjustment methods to use for FET
FET.P.ADJUST.METHODS <- c("bonferroni",
                          "fdr",
                          "BY",
                          "none")

# The accessor functions used for the gene deregulation types
FET.GENE.DEREGULATION.GET.GENES.FUNCS <- list(lo=getSigGenesList.lo.sam,
                                              up=getSigGenesList.up.sam)

# The gene deregulation types to run fet analysis for
FET.GENE.DEREGULATION.TYPES <- names(FET.GENE.DEREGULATION.GET.GENES.FUNCS)

# --------------
# Run Fisher Exact Test

for (dataSet.name in DATA.SET.NAMES) {
  for (ontology.name in ONTOLOGY.NAMES) {
    for (sam.fdr in SAM.FDRS.MAX) {
      for (sam.ts in SAM.TEST.STATISTICS) {
        for (sam.np in SAM.NPERMS) {
          for (fet.fdr in FET.FDRS.MAX) {
            for (fet.p.adj.method in FET.P.ADJUST.METHODS) {
              for (fet.dereg in FET.GENE.DEREGULATION.TYPES) {
                lArgs <- list(dataSet.name=dataSet.name,
                              dataSet.mods=DATA.SET.MODS[dataSet.name],
                              dataSet.inClass=getSamClass(),
                              dataSet.inQuals = generateQuals.sam(testStatistic=sam.ts,
                                                                  fdr.output=sam.fdr,
                                                                  nperms=sam.np,
                                                                  dataSet.inQuals="",
                                                                  dataSet.inClass=""),
                              probeIdToGeneIdMap=PROBE.MAPS[[DATA.SET.PROBE.MAP.KEYS[dataSet.name]]],
                              pathIdToPathNameFunc=PATHID2PATHNAME.FUNCS[[ONTOLOGY.PATHID2PATHNAME.FUNC.KEYS[ontology.name]]],
                              out.ontology.name=ontology.name,
                              fet.fdr=fet.fdr,
                              fet.p.adj.method=fet.p.adj.method,
                              fet.getSigGenesFunc=FET.GENE.DEREGULATION.GET.GENES.FUNCS[[fet.dereg]],
                              fet.dereg=fet.dereg)
                
                print("_____________")
                print("RUNNING FET")
                print("_____________")
                print(str(lArgs))
                runFet(lArgs)
              } # end iteration over FET gene deregulation types
            } # end iteration over FET p-value adjust methods
          } # end iteration over FET fdrs
        } # end iteration of SAM number of permutations
      } # end iteration over SAM test statistics
    } # end iteration over SAM fdrs
  } # end iteration over ontologies
} # end iteration over data sets
