################################################################################################################################################
# GSM Policy: FAIME
################################################################################################################################################

source("./src/shell/R/configPaths.R")
source("./src/shell/R/runFaime.R")
source("./src/shell/R/runSam.R")
source("./src/shell/R/gsmIterator.R")
source("./src/shell/R/utilsKegg.R")
source("./src/shell/R/utilsGo.R")

# The transformations used to generate FAIME scores
GSM.FAIME.TRANSFORM.MASK = c("faimeTransform.RankedNegExp"=TRUE,
                             "faimeTransform.RankedPosExp"=FALSE,
                             "faimeTransform.Ranked"=FALSE,
                             "faimeTransform.None"=FALSE)

# The names of the transformation functions
GSM.FAIME.TRANSFORM.FUNC.NAMES <- names(GSM.FAIME.TRANSFORM.MASK[GSM.FAIME.TRANSFORM.MASK == TRUE])

# The reductions used to generate FAIME scores
GSM.FAIME.REDUCE.MASK <- c("faimeReduce.CentroidDistance"=TRUE,
                           "faimeReduce.Centroid"=FALSE,
                           "faimeReduce.MedianDistance"=FALSE,
                           "faimeReduce.Median"=FALSE)

# The names of the reduction functions
GSM.FAIME.REDUCE.FUNC.NAMES <- names(GSM.FAIME.REDUCE.MASK[GSM.FAIME.REDUCE.MASK == TRUE])

# The reference FDR to derive maximum FDRs from
GSM.FAIME.SAM.REF.FDR <- 0.45

# The test statistics to use mask
GSM.FAIME.SAM.TEST.STATISTICS.MASK <- c("standard"=FALSE,
                                        "wilcoxon"=TRUE)

# The test statistics to use 
GSM.FAIME.SAM.TEST.STATISTICS <- names(GSM.FAIME.SAM.TEST.STATISTICS.MASK[GSM.FAIME.SAM.TEST.STATISTICS.MASK==TRUE])

# The number of permutations to use for FDR estimation
GSM.FAIME.SAM.NPERMS <- c(1000)

#' @return Policy identifier
gsmGetPolicyName.Faime <- function() { "FAIME" }

#' @return Iterator set to start of parameterized values
gsmGetBeginInputIterator.Faime <- function() {
  gsmCreateIterator(list(transform.funcName=GSM.FAIME.TRANSFORM.FUNC.NAMES,
                         reduce.funcName=GSM.FAIME.REDUCE.FUNC.NAMES,
                         sam.testStatistic=GSM.FAIME.SAM.TEST.STATISTICS,
                         sam.nperms=GSM.FAIME.SAM.NPERMS))
}

#' @return List of input arguments to Gsm
gsmCreateInputArgs.Faime <- function(handle, common.args, input.iter) {
  iter.args <- gsmDereferenceIterator(input.iter)
  input.args <- common.args
  input.args$dataSet.inClass <- getSamClass()
  input.args$dataSet.inQuals <- generateQuals.sam(testStatistic=iter.args$sam.testStatistic,
                                                  fdr.output=handle$gsmFaimeSamRefFdr,
                                                  nperms=iter.args$sam.nperms,
                                                  dataSet.inQuals="",
                                                  dataSet.inClass=getFaimeClass())
  input.args$transform.funcName <- iter.args$transform.funcName
  input.args$reduce.funcName <- iter.args$reduce.funcName
  input.args$handle <- handle
  return(input.args)
}

#' @return Policy handle primed to compute input metrics
gsmInitInputMetrics.Faime <- function(handle, input.args) {
  # Load input data according to parameters
  sam.path <- getDataSetInputPath(input.args)
  sam.result <- get(load(sam.path))
  
  # Determine deregulation accessor
  dereg.accessor.name <- paste("getSigGenesTable",
                               input.args$dereg,
                               "sam",
                               sep=".")
  dereg.accessor <- get(dereg.accessor.name)
  stopifnot(!is.null(dereg.accessor))
  
  # Store deregulated data within handle
  sam.paths <- dereg.accessor(sam.result)
  sam.paths <- orderByQValueAscThenByScoreDesc.sam(sam.paths)
  handle$dataSet.input.data <- sam.paths
  return(handle)
}

#' @return Vector of input positives at parameter false discovery rate
gsmGetInputPositives.Faime  <- function(handle, input.args, fdr) {
  term.id.col.name.accessor.name <- paste(input.args$ontology.key,
                                          "IdColName",
                                          sep="")
  term.id.col.name.accessor <- get(term.id.col.name.accessor.name)
  stopifnot(!is.null(term.id.col.name.accessor))
  input.matrix <- filterByFdr.sam(handle$dataSet.input.data, fdr, bIsPreSorted=TRUE)
  # BEGIN HACK: R for some reason turns a single row matrix into a character vector
  if (class(input.matrix) == "matrix") {
    input.positives <- input.matrix[ , term.id.col.name.accessor()]
  } else {
    stopifnot(class(input.matrix) == "character")
    input.positives <- input.matrix[term.id.col.name.accessor()]
  }
  # END HACK
  names(input.positives) <- NULL
  return(input.positives)
}

#' @return Policy handle
gsmCreatePolicy.Faime <- function() {
  list(gsmGetPolicyName=gsmGetPolicyName.Faime,
       gsmGetBeginInputIterator=gsmGetBeginInputIterator.Faime,
       gsmCreateInputArgs=gsmCreateInputArgs.Faime,
       gsmInitInputMetrics=gsmInitInputMetrics.Faime,
       gsmGetInputPositives.Faime=gsmGetInputPositives.Faime,
       gsmFaimeSamRefFdr=GSM.FAIME.SAM.REF.FDR)
}

#' @return A factory which can create a policy handle
getGsmPolicyFactoryFaime <- function() {
  list(createPolicy=gsmCreatePolicy.Faime)
}
