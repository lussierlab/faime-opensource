################################################################################################################################################
# Utility to run SAM analysis on FAIME data
################################################################################################################################################

# Load SAM
library("samr")

source("./src/shell/R/configPaths.R")
source("./src/shell/R/utilsRun.R")

# --------------

#' @return Character vector of the SAM class
getSamClass <- function() { "SAM" }

#' Utility to sort significant pathways/genes by q-value (fdr) ascending and Score descending 
#' @param sam.paths A SAM significant paths/genes matrix
#' @return A SAM significant paths/genes matrix sorted by q-value ascending and score descending, will also remap rows to account for new order
orderByQValueAscThenByScoreDesc.sam <- function(sam.paths) {
  if (!is.null(sam.paths)) {
    sam.paths <- sam.paths[order(as.numeric(sam.paths[,"q-value(%)"]),
                                -abs(as.numeric(sam.paths[,"Score(d)"]))),]
    rownames(sam.paths) <- 1:nrow(sam.paths)
  }
  return(sam.paths)
}

#' Sorts pathways/genes and then returns pathways below threshold
#' @param sam.paths A SAM significant paths/genes matrix filtered by FDR
#' @param flt.fdr false discovery rate to filter by
#' @param bIsPreSorted TRUE if we can skip sorting
#' @return SAM significant paths/genes matrix less than FDR
filterByFdr.sam <- function(sam.paths, flt.fdr, bIsPreSorted=FALSE) {
  if (!is.null(sam.paths)) {
    if (!bIsPreSorted) {
      sam.paths <- orderByQValueAscThenByScoreDesc.sam(sam.paths)
    }
    sam.paths <- sam.paths[ as.numeric(sam.paths[, "q-value(%)"]) < (100.0 * flt.fdr), ]
  }
  return(sam.paths)
}

#' Utility which will map probe identifiers to gene identifiers
#' @param sam.result SAM output
#' @param probeIdToGeneIdMap Mapping from probe identifier to gene identifier
#' @return modified sam.result with probe identifiers mapped to gene identifiers
mapProbeIdsToGeneIds.sam <- function(sam.result, probeIdToGeneIdMap) {
  probeIdToGeneIdMap <- as.data.frame(as.matrix(probeIdToGeneIdMap), stringsAsFactors = FALSE)
  mapGenes <- function(genes, probeIdToGeneIdMap) {
    if (!is.null(genes)) {
      genes <- as.data.frame(genes, stringsAsFactors = FALSE)
      genes$"Gene ID" <- genes$"Gene Name"
      genes <- merge(genes,
                     probeIdToGeneIdMap,
                     by.x="Gene Name",
                     by.y="row.names")
      genes$"Gene Name" <- genes[[ncol(genes)]]
      genes <- as.matrix(subset(genes, select=-c(ncol(genes))))
    }
    return(genes)
  }
  sam.result$siggenes.table$genes.lo <- orderByQValueAscThenByScoreDesc.sam(mapGenes(sam.result$siggenes.table$genes.lo, probeIdToGeneIdMap))
  sam.result$siggenes.table$genes.up <- orderByQValueAscThenByScoreDesc.sam(mapGenes(sam.result$siggenes.table$genes.up, probeIdToGeneIdMap))
  return(sam.result)
}

#' Utility which will map path identifiers to path names
#' @param sam.result SAM output
#' @param pathIdToPathNameFunc Function for mapping a set of path identifiers to path names
#' @return modified sam.result with path identifiers mapped to path names
mapPathIdsToPathNames.sam <- function(sam.result, pathIdtoPathNameFunc) {
  mapPaths <- function(paths, pathIdtoPathNameFunc) {
    if (!is.null(paths)) {
      paths <- as.data.frame(paths, stringsAsFactors = FALSE)
      paths <- pathIdtoPathNameFunc(paths, by.y="Gene Name")
      paths <- as.matrix(subset(paths, select=-which(colnames(paths)=="Gene ID")))
    }
    return(paths)
  }
  sam.result$siggenes.table$genes.lo <- orderByQValueAscThenByScoreDesc.sam(mapPaths(sam.result$siggenes.table$genes.lo, pathIdtoPathNameFunc))
  sam.result$siggenes.table$genes.up <- orderByQValueAscThenByScoreDesc.sam(mapPaths(sam.result$siggenes.table$genes.up, pathIdtoPathNameFunc))
  return(sam.result)
}

#' Conditionally writes significant genes csv file if genes are not null
#' @param sam.result The output of SAM
#' @param dereg Either "up" or "lo" to identify which genes
#' @param output.dir The output directory
#' @param lArgs Parameters to derive file name from
condWriteSigGenesCsv.sam <- function(sam.result, dereg="up", output.dir, lArgs) {
  genes <- sam.result$siggenes.table[[sprintf("genes.%s", dereg)]]
  write.csv(genes, file=sprintf("%s/%s-%s.csv", output.dir, getDataSetOutputName(lArgs), dereg))
}

#' Utility function to generate the SAM qualifiers
#' @param testStatistic The SAM test statistic
#' @param fdr.output The threshold false discovery rate
#' @param nperms The number of permuations used for estimating FDR
#' @param dataSet.inQuals Input qualifiers
#' @param dataSet.inClass Input class
generateQuals.sam <- function(testStatistic, fdr.output, nperms, dataSet.inQuals, dataSet.inClass) {
  sprintf("%s%s%s.ts%s.fdr%s.np",
          condDashPrefix(condSetDefault(dataSet.inQuals, "")),
          condDashPrefix(condSetDefault(dataSet.inClass, "")),
          condDashPrefix(condSetDefault(testStatistic, "")),
          condDashPrefix(condSetDefault(fdr.output, "")),
          condDashPrefix(condSetDefault(nperms, "")))
}

#' @param sam.result SAM object returned by SAM analysis
#' @return A list of down-regulated significant genes
getSigGenesList.lo.sam <- function (sam.result) {
  tmp <- getSigGenesTable.lo.sam(sam.result)
  tmp[,"Gene Name"]
}

#' @param sam.result SAM object returned by SAM analysis
#' @return A list of up-regulated significant genes
getSigGenesList.up.sam <- function (sam.result) {
  tmp <- getSigGenesTable.up.sam(sam.result)
  tmp[,"Gene Name"]
}

#' @param sam.result SAM object returned by SAM analysis
#' @return A table of down-regulated significant genes
getSigGenesTable.lo.sam <- function (sam.result) {
  sam.result$siggenes$genes.lo
}

#' @param sam.result SAM object returned by SAM analysis
#' @return A table of up-regulated significant genes
getSigGenesTable.up.sam <- function (sam.result) {
  sam.result$siggenes$genes.up
}

#' Utility function based on path convention for running a SAM analysis
#' @param lArgs A list of arguments containing dataSet.name, dataSet.mods, dataSet.inClass,
#'          ontology.name, transform.funcName, input.dir, output.dir, probeIdToGeneIdMap, pathIdToPathNameFunc,
#'          sam.testStatistic, sam.fdr.output, sam.resp.type, sam.nperms
#' @return FAIME scores for arguments
#' @example
#' sam.result.GSE2379.GoMf <- runSam(list(dataSet.name="GSE2379",
#                                         dataSet.mods="-filtered",
#                                         dataSet.inClass="FAIME"
#                                         ontology.name="GOMF-gene",
#                                         transform.funcName="faimeTransform.RankedNegExp",
#                                         sam.resp.type="Two class unpaired"))
runSam <- function( lArgs ) {
  # Make sure problem type is specified
  if (is.null(lArgs$sam.resp.type)) {
    stop("SAM needs to know if data is paired")
  }

  # Derive any missing parameters
  lArgs$sam.testStatistic <- condSetDefault(lArgs$sam.testStatistic, "standard")
  lArgs$sam.fdr.output <- condSetDefault(lArgs$sam.fdr.output, 0.05)
  lArgs$sam.nperms <- condSetDefault(lArgs$sam.nperms, 1000)
  lArgs$dataSet.outQuals <- condSetDefault(lArgs$dataSet.outQuals,
                                           generateQuals.sam(testStatistic=lArgs$sam.testStatistic,
                                                             fdr.output=lArgs$sam.fdr.output,
                                                             nperms=lArgs$sam.nperms,
                                                             dataSet.inQuals=lArgs$dataSet.inQuals,
                                                             dataSet.inClass=lArgs$dataSet.inClass))
  lArgs$dataSet.outClass <- condSetDefault(lArgs$dataSet.outClass, getSamClass())
  
  # Determine output directory
  output.dir <- getParamsOutputDir(lArgs)
  condCreateDir(output.dir)
  
  # Load input scores
  dataSet.scores <- readDataSetScores(lArgs)
  
  # Read in phenotype information
  pheno.values <- readAndVerifyDataSetPhenoValues(lArgs$dataSet.name, dataSet.scores)
  
  # Run SAM
  print("Running SAM:")
  sam.result <- SAM(x=dataSet.scores,
                    y=as.numeric(as.matrix(pheno.values["SamClass",])),
                    resp.type=lArgs$sam.resp.type,
                    geneid=rownames(dataSet.scores),
                    fdr.output=lArgs$sam.fdr.output,
                    testStatistic=lArgs$sam.testStatistic,
                    nperms=lArgs$sam.nperms)
  print(sam.result)
  
  # Patch up significant genes table for microarrays
  if (!is.null(lArgs$probeIdToGeneIdMap)) {
    sam.result <- mapProbeIdsToGeneIds.sam(sam.result, lArgs$probeIdToGeneIdMap)
  }
  
  # Patch up significant genes table for pathways
  if (!is.null(lArgs$pathIdToPathNameFunc)) {
    sam.result <- mapPathIdsToPathNames.sam(sam.result, lArgs$pathIdToPathNameFunc)
  }
  
  # Write SAM result (only keeping siggenes.table)
  sam.result <- list(siggenes.table=sam.result$siggenes.table)
  save(sam.result, file=getDataSetOutputPath(lArgs))
  condWriteSigGenesCsv.sam(sam.result, "lo", output.dir, lArgs)
  condWriteSigGenesCsv.sam(sam.result, "up", output.dir, lArgs)
  
  # Return SAM output
  return(sam.result)
}
