################################################################################################################################################
# GO Utilities
################################################################################################################################################

#' @return data frame with first column being GO identifier and second column being the pathway name
goId2NameTable <- function(file="./src/shell/resources/ontologies/GO_ID_TO_NAME_TABLE_6_14_12.txt") {
  if (!exists("cachedGoIdToNameTable", inherits=TRUE)) {
    cachedGoIdToNameTable <<- read.table(file=file, sep="\t", header=TRUE, colClasses="character", quote = "")
  }
  return(cachedGoIdToNameTable)
}

#' @return Character vector with name of column containing identifiers for GO terms
goIdColName <- function() { "GOID" }

#' @return Character vector with name of column containing names for GO terms
goNameColName <- function() { "Name" }

#' @param ids a list or vector of GO identifiers
#' @param by.y column name of GO identifiers to merge by
#' @param id2NameTable table mapping identifiers to names
#' @return data frame with first column being GO identifier and second column being the pathway name
goId2Name <- function(ids, by.y=1, id2NameTable=goId2NameTable()) {
  if (!is.null(ids)) {
    merge(id2NameTable,
          as.data.frame(ids),
          by.x=goIdColName(),
          by.y=by.y)
  }
}

