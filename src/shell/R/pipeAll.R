################################################################################################################################################
# Research Pipeline: Runs All Pipelines
################################################################################################################################################

# Generate FAIME scores
source("./src/shell/R/pipeFaime.R")

# Generate significant FAIME pathways
source("./src/shell/R/pipeSigFaime.R")

# Generate significant genes
source("./src/shell/R/pipeSigGenes.R")

# Generate significant FET pathways
source("./src/shell/R/pipeFet.R")

# Generate FAIME-FAIME overlap
source("./src/shell/R/pipeOverlapFaime.R")

# Generate FET-FET overlap
source("./src/shell/R/pipeOverlapFet.R")

# Generate GSEA
source("./src/shell/R/pipeGsea.R")
