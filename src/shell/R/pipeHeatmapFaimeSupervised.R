################################################################################################################################################
# Research Pipeline: Generate FAIME Heatmaps
################################################################################################################################################

source("./src/shell/R/configPaths.R")
source("./src/shell/R/runFaime.R")
source("./src/shell/R/runHeatmapFaime.R")
source("./src/shell/R/runSam.R")
source("./src/shell/R/utilsGo.R")
source("./src/shell/R/utilsKegg.R")

# Sig = Data set to use significant features from to categorize Scores
DATA.SET.GROUPS <- list( list(sig="GSE36968", scores="GSE13861"),
                         list(sig="GSE13861", scores="GSE36968") )

# Mapping from data set to its modifiers
DATA.SET.MODS <- c("GSE2379"="-filtered",
                   "GSE6631"="-filtered",
                   "E-MEXP-44"="-filtered",
                   "GSE36968"="-rpkm",
                   "GSE13861"="-norm")

# Mask used to filter out which ontologies to use
ONTOLOGY.MASK <- c("KEGG"=TRUE,
                   "GO_molecular_function_6_14_12"=TRUE,
                   "GO_biological_process_6_14_12"=TRUE,
                   "GOMF-gene"=TRUE,
                   "GOBP-gene"=TRUE,
                   "GOBP-gene-500-5"=TRUE)

# The names of the ontologies to use
ONTOLOGY.NAMES <- names(ONTOLOGY.MASK[ONTOLOGY.MASK == TRUE])

# The transformations used to generate FAIME scores
TRANSFORM.MASK = c("faimeTransform.RankedNegExp"=TRUE,
                   "faimeTransform.RankedPosExp"=FALSE,
                   "faimeTransform.Ranked"=FALSE,
                   "faimeTransform.None"=FALSE)

# The names of the transformation functions
TRANSFORM.FUNC.NAMES <- names(TRANSFORM.MASK[TRANSFORM.MASK == TRUE])

# The reductions used to generate FAIME scores
REDUCE.MASK <- c("faimeReduce.CentroidDistance"=TRUE,
                 "faimeReduce.Centroid"=TRUE,
                 "faimeReduce.MedianDistance"=FALSE,
                 "faimeReduce.Median"=FALSE)

# The names of the reduction functions
REDUCE.FUNC.NAMES <- names(REDUCE.MASK[REDUCE.MASK == TRUE])

# Keys specifying which ontology path id to path name mapping to use
ONTOLOGY.ATTRIB.KEYS <- c("KEGG"="kegg",
                          "GO_molecular_function_6_14_12"="go",
                          "GO_biological_process_6_14_12"="go",
                          "GOMF-gene"="go",
                          "GOBP-gene"="go",
                          "GOBP-gene-500-5"="go")

ONTOLOGY.LABELS <- c("KEGG"="KEGG Pathways",
                     "GO_molecular_function_6_14_12"="GO Molecular Function",
                     "GO_biological_process_6_14_12"="GO Biological Process",
                     "GOMF-gene"="GO Molecular Function",
                     "GOBP-gene"="GO Biological Process",
                     "GOBP-gene-500-5"="GO Biological Process")

# The name of the column containing the ontology identifiers
PATHID.COL.NAMES <- c(go=goIdColName(),
                      kegg=keggIdColName())

# The name of the column containing the ontology identifiers
PATHNAME.COL.NAMES <- c(go=goNameColName(),
                        kegg=keggNameColName())

# The reference FDR to derive maximum FDRs from
SAM.REF.FDR <- 0.45

# The various False Discovery Rate thresholds
SAM.FDRS.MAX.MASK <- c('0.01'=TRUE,
                       '0.02'=TRUE,
                       '0.03'=TRUE,
                       '0.04'=TRUE,
                       '0.05'=TRUE)

# The various False Discovery Rate thresholds
SAM.FDRS.MAX <- as.numeric(names(SAM.FDRS.MAX.MASK[SAM.FDRS.MAX.MASK==TRUE]))

# The mask specifying the test statistics of interest
SAM.TEST.STATISTICS <- c("standard"=FALSE,
                         "wilcoxon"=TRUE)

# The test statistics to use 
SAM.TEST.STATISTICS <- names(SAM.TEST.STATISTICS[SAM.TEST.STATISTICS == TRUE])

# The number of permutations to use for FDR estimation
SAM.NPERMS <- c(1000)

sam.filter.ref.fdr.lo <- function(sam.result, lArgs) {
  filterByFdr.sam(getSigGenesTable.lo.sam(sam.result), lArgs$flt.fdr)
}

sam.filter.ref.fdr.up <- function(sam.result, lArgs) {
  filterByFdr.sam(getSigGenesTable.up.sam(sam.result), lArgs$flt.fdr)
}

# The accessor functions used for the gene deregulation types
HEAT.PATH.DEREGULATION.GET.PATHS.FUNCS <- list(lo=sam.filter.ref.fdr.lo,
                                               up=sam.filter.ref.fdr.up)

# Mask of clustering methods
HEAT.HCLUST.METHODS.MASK <- c("ward"=TRUE,
                              "single"=TRUE,
                              "complete"=TRUE,
                              "average"=TRUE,
                              "mcquitty"=TRUE,
                              "median"=TRUE,
                              "centroid"=TRUE,
                              "none"=TRUE)

# The final set of clustering methods to use
HEAT.HCLUST.METHODS <- names(HEAT.HCLUST.METHODS.MASK[HEAT.HCLUST.METHODS.MASK == TRUE])

# --------------
# Run analysis

# Variable for storing all overlap results
ov.results <- data.frame()

for (dataSet.group in DATA.SET.GROUPS) {
  for (ontology.name in ONTOLOGY.NAMES) {
    for (transform.funcName in TRANSFORM.FUNC.NAMES) {
      for (reduce.funcName in REDUCE.FUNC.NAMES) {
        for (sam.fdr in SAM.FDRS.MAX) {
          for (sam.ts in SAM.TEST.STATISTICS) {
            for (sam.np in SAM.NPERMS) {
              for (heat.hclust.method in HEAT.HCLUST.METHODS) {
                # Arguments for SAM data
                additional.desc <- sprintf("%s.flt.fdr", sam.fdr)
                lArgs.sig <- list(dataSet.name=dataSet.group$sig,
                                  dataSet.inClass=getSamClass(),
                                  dataSet.mods=DATA.SET.MODS[dataSet.group$sig],
                                  dataSet.inQuals=generateQuals.sam(testStatistic=sam.ts,
                                                                    fdr.output=SAM.REF.FDR,
                                                                    nperms=sam.np,
                                                                    dataSet.inQuals="",
                                                                    dataSet.inClass=getFaimeClass()),
                                  ontology.name=ontology.name,
                                  transform.funcName=transform.funcName,
                                  reduce.funcName=reduce.funcName,
                                  heat.unsupervised=FALSE,
                                  heat.id.key=PATHID.COL.NAMES[ONTOLOGY.ATTRIB.KEYS[ontology.name]],
                                  heat.name.key=PATHNAME.COL.NAMES[ONTOLOGY.ATTRIB.KEYS[ontology.name]],
                                  heat.dereg.funcs=HEAT.PATH.DEREGULATION.GET.PATHS.FUNCS,
                                  heat.hclust.method=heat.hclust.method,
                                  heat.Rowv=NA,
                                  heat.main=paste(dataSet.group$scores, ONTOLOGY.LABELS[ontology.name], sep=": "),
                                  flt.fdr=sam.fdr,
                                  additional.desc=additional.desc)
                # Determine clustering method
                clust.key <- "heat.Colv"
                clust.val <- NA
                if (heat.hclust.method != "none") {
                  clust.key <- "heat.hclustfun"
                  clust.val <- function(d, members=NULL) { hclust(d, method=heat.hclust.method, members) }
                }
                lArgs.sig[[clust.key]] <- clust.val  
              
                # Arguments for FAIME scores
                lArgs.scores <- list(dataSet.name=dataSet.group$scores,
                                     dataSet.inClass=getFaimeClass(),
                                     dataSet.mods=DATA.SET.MODS[dataSet.group$scores],
                                     dataSet.outSubClass="supervised",
                                     ontology.name=ontology.name,
                                     transform.funcName=transform.funcName,
                                     reduce.funcName=reduce.funcName)
                             
                # Run heatmap
                print("_____________")
                print("RUNNING FAIME HEATMAP")
                print("_____________")
                print(str(lArgs.sig))
                print(str(lArgs.scores))
                runHeatmapFaime(lArgs.sig,
                                lArgs.scores)
              } # end iteration over clustering methods
            } # end iteration of number of permutations
          } # end iteration over test statistics
        } # end iteration over fdrs
      } # end iteration over reductions
    } # end iteration over transforms
  } # end iteration over ontologies
} # end iteration over data sets
