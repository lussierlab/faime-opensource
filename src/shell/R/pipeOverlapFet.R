################################################################################################################################################
# Research Pipeline: Generate FET-FET Overlap
################################################################################################################################################

source("./src/shell/R/configPaths.R")
source("./src/shell/R/runFet.R")
source("./src/shell/R/runOverlap.R")
source("./src/shell/R/runSam.R")
source("./src/shell/R/utilsGo.R")
source("./src/shell/R/utilsKegg.R")

# Groups of datasets to compare overlap
DATA.SET.GROUPS <- list( c("GSE36968","GSE13861") )

# Mapping from data set to its modifiers
DATA.SET.MODS <- c("GSE2379"="-filtered",
                   "GSE6631"="-filtered",
                   "E-MEXP-44"="-filtered",
                   "GSE36968"="-rpkm",
                   "GSE13861"="-norm")

# Mask used to filter out which ontologies to use
ONTOLOGY.MASK <- c("KEGG"=TRUE,
                   "GO_molecular_function_6_14_12"=TRUE,
                   "GO_biological_process_6_14_12"=TRUE,
                   "GOMF-gene"=TRUE,
                   "GOBP-gene"=TRUE,
                   "GOBP-gene-500-5"=TRUE)

# The names of the ontologies to use
ONTOLOGY.NAMES <- names(ONTOLOGY.MASK[ONTOLOGY.MASK == TRUE])

# Keys specifying which ontology path id to path name mapping to use
ONTOLOGY.ATTRIB.KEYS <- c("KEGG"="kegg",
                          "GO_molecular_function_6_14_12"="go",
                          "GO_biological_process_6_14_12"="go",
                          "GOMF-gene"="go",
                          "GOBP-gene"="go",
                          "GOBP-gene-500-5"="go")

# Functions for mapping path identifiers to path names
PATHID2PATHNAME.FUNCS <- list(go=goId2Name,
                              kegg=keggId2Name)

# The name of the column containing the ontology identifiers
PATHID.COL.NAMES <- c(go=goIdColName(),
                      kegg=keggIdColName())

# The various False Discovery Rate thresholds
SAM.FDRS.MAX <- c(0.01,
                  0.05)

# The test statistics to use 
SAM.TEST.STATISTICS <- c("standard",
                         "wilcoxon")

# The number of permutations to use for FDR estimation
SAM.NPERMS <- c(1000)

# The reference FDR to derive maximum FDRs from
FET.REF.FDR <- 0.45

# The various false discovery rate thresholds for FET
FET.FDRS.MAX <- c(0.01,
                  0.05)

# The various p-value adjustment methods to use for FET
FET.P.ADJUST.METHODS <- c("bonferroni",
                          "fdr",
                          "BY",
                          "none")

# Filters fet by target FDR
fet.filter.ref.fdr <- function(fet.result, lArgs) {
    filterByFdr.fet(fet.result, lArgs$flt.fdr)
}

# The accessor functions used for the gene deregulation types
OV.PATH.DEREGULATION.GET.PATHS.FUNCS <- list(lo=fet.filter.ref.fdr,
                                             up=fet.filter.ref.fdr)

# The gene deregulation types
OV.PATH.DEREGULATION.TYPES <- names(OV.PATH.DEREGULATION.GET.PATHS.FUNCS)

# --------------
# Run analysis

# Variable for storing all overlap results
ov.results <- data.frame()

for (grp in DATA.SET.GROUPS) {
  for (ontology.name in ONTOLOGY.NAMES) {
    for (sam.fdr in SAM.FDRS.MAX) {
      for (sam.ts in SAM.TEST.STATISTICS) {
        for (sam.np in SAM.NPERMS) {
          for (fet.fdr in FET.FDRS.MAX) {
            for (fet.p.adj.met in FET.P.ADJUST.METHODS) {
              for (ov.dereg in OV.PATH.DEREGULATION.TYPES) {
                additional.desc <- sprintf("%s.flt.fdr", fet.fdr)
                # SAM qualifiers
                samQuals <- generateQuals.sam(testStatistic=sam.ts,
                                              fdr.output=sam.fdr,
                                              nperms=sam.np,
                                              dataSet.inQuals="",
                                              dataSet.inClass="")
                # FET qualifiers
                fetQuals <- generateQuals.fet(fdr=FET.REF.FDR,
                                              p.adj.method=fet.p.adj.met,
                                              dereg=ov.dereg,
                                              dataSet.inQuals=samQuals,
                                              dataSet.inClass=getSamClass())
                # Arguments for first data set
                lArgs1 <- list(dataSet.name=grp[1],
                               dataSet.inClass=getFetClass(),
                               dataSet.mods=DATA.SET.MODS[grp[1]],
                               dataSet.inQuals=fetQuals,
                               ontology.name=ontology.name,
                               ov.key=PATHID.COL.NAMES[ONTOLOGY.ATTRIB.KEYS[ontology.name]],
                               ov.getDeregFunc=OV.PATH.DEREGULATION.GET.PATHS.FUNCS[[ov.dereg]],
                               ov.dereg=ov.dereg,
                               flt.fdr=fet.fdr,
                               additional.desc=additional.desc)
                # Arguments for second data set
                lArgs2 <- lArgs1
                lArgs2$dataSet.name <- grp[2]
                lArgs2$dataSet.mods <- DATA.SET.MODS[grp[2]]
                # Run overlap                
                print("_____________")
                print("RUNNING FET OVERLAP")
                print("_____________")
                print(str(lArgs1))
                print(str(lArgs2))
                ov.result <- runOverlap2(lArgs1,
                                         lArgs2,
                                         ov.type="FET-FET",
                                         pathIdToPathNameFunc=PATHID2PATHNAME.FUNCS[[ONTOLOGY.ATTRIB.KEYS[ontology.name]]])
                # Append additional data of interest
                ov.insert <- c(ontology = ontology.name,
                               FET.P.ADJ.METH = fet.p.adj.met,
                               FET.FDR = fet.fdr,
                               SAM.GENES.FDR = sam.fdr,
                               SAM.test = sam.ts)
                ov.result <- append(x=ov.result, values=ov.insert, after=2)
                # Bind results
                ov.results <- rbind(ov.results, as.data.frame(ov.result, stringsAsFactors=FALSE))
              } # end iteration over deregulation types
            } # end iteration over p-value adjustment method
          } # end iteration over fet fdrs
        } # end iteration of number of permutations
      } # end iteration over test statistics
    } # end iteration over sam fdrs
  } # end iteration over ontologies
} # end iteration over data set groups

# Write overlaps
condCreateDir(getDefaultAggregateOverlapDir())
file.basePath <- sprintf("%s/%s-%s-%s-%s-%s",
                         getDefaultAggregateOverlapDir(),
                         getFetClass(),
                         getSamClass(),
                         getFetClass(),
                         getSamClass(),
                         getOverlap2Class())
save(ov.results, file=sprintf("%s.rdata", file.basePath))
write.csv(x=ov.results, file=sprintf("%s.csv", file.basePath))
